<?php

namespace Tests\Unit\Notifications;

use App\Models\User;
use App\Events\UserCreated;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Notifications\NewCredentials;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class NewCredentialsTest extends TestCase
{
    use RefreshDatabase;

    public function testNotifiedOnUserCreatedEvent()
    {
        $fake = Notification::fake();

        $password = 'chiki-briki';
        $user = User::factory()->create([
            'password' => Hash::make($password)
        ]);

        event(new UserCreated($user, $password));

        $fake->assertSentTo($user, NewCredentials::class);
    }
}
