<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, CreatesActor;

    protected function setUpTraits()
    {
        $uses = parent::setUpTraits();

        if (isset($uses[SeedsPermissions::class])) {
            $this->setUpPermissions();
        }

        return $uses;
    }
}
