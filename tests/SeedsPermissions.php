<?php

namespace Tests;

trait SeedsPermissions
{
    public function setUpPermissions()
    {
        $this->artisan('db:seed', [
            '--class' => 'PermissionsSeeder'
        ]);
    }
}
