<?php

namespace Tests;

use App\Models\User;

trait CreatesActor
{
    protected function createUser(array $data = []): User
    {
        return User::factory()->create($data);
    }

    protected function createActor(array $data = []): User
    {
        $user = $this->createUser($data);
        $this->actingAs($user, $this->defaultActorGuard());
        return $user;
    }

    protected function defaultActorGuard(): ?string
    {
        return 'web';
    }
}
