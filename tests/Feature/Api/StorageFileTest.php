<?php

namespace Tests\Feature\Api;

use App\Models\StorageFile;
use App\Models\User;
use App\Events\StorageFileUploaded;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class StorageFileTest extends TestCase
{
    use RefreshDatabase;

    protected $resourceStructure = [
        'data' => [
            'id',
            'filename',
            'extension',
            'mime_type',
            'uploaded_at',
            'uploader'
        ]
    ];

    protected $disk;

    public function setUp(): void
    {
        parent::setUp();

        $this->disk = config('filesystems.default', 'dummy');
        Storage::fake($this->disk);
    }

    public function testUploadWithoutFile()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user, 'api')->json('post', route('storage.files.upload'));
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['file']);
    }

    public function testUploadValidImage()
    {
        $event = Event::fake();

        $user = User::factory()->create();

        $uploaded = UploadedFile::fake()->image('image.jpeg');

        $response = $this->actingAs($user, 'api')->json('post', route('storage.files.upload'), [
            'file' => $uploaded
        ]);

        $response->assertStatus(201);
        $response->assertJsonStructure($this->resourceStructure);
        $response->assertJson([
            'data' => [
                'filename' => 'image',
                'extension' => 'jpeg',
                'mime_type' => 'image/jpeg',
                'uploader' => [
                    'id' => $user->id,
                ]
            ]
        ]);

        Storage::disk($this->disk)->assertExists($uploaded->hashName('files'));

        $event->assertDispatched(StorageFileUploaded::class);
    }

    public function testMeta()
    {
        $user = User::factory()->create();

        $file = $this->createAndUploadFileInternally(
            $user, UploadedFile::fake()->image('image.png')
        );

        $response = $this->actingAs($user, 'api')->json('get', route('storage.files.meta', ['file' => $file->id]));

        $response->assertStatus(200);
        $response->assertJsonStructure($this->resourceStructure);
    }

    public function testDownload()
    {
        $user = User::factory()->create();

        $file = $this->createAndUploadFileInternally(
            $user, UploadedFile::fake()->create('document.pdf', 60 * 60 * 60)
        );

        $response = $this->actingAs($user, 'api')->get(route('storage.files.download', ['file' => $file->id]));

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/pdf');

        $filename = Str::ascii($file->real_filename . '.' . $file->extension);
        $response->assertHeader('Content-Disposition', 'attachment; filename=' . $filename);
    }

    public function testUnlink()
    {
        $user = User::factory()->create();

        $file = $this->createAndUploadFileInternally(
            $user, $uploaded = UploadedFile::fake()->create('document.xlsx', 60 * 60 * 60)
        );

        Storage::disk($this->disk)->assertExists($uploaded->hashName('files'));

        $response = $this->actingAs($user, 'api')->json('delete', route('storage.files.unlink', ['file' => $file->id]));
        $response->assertStatus(200);

        Storage::disk($this->disk)->assertMissing($uploaded->hashName('files'));
    }

    protected function createAndUploadFileInternally(User $user, UploadedFile $uploaded)
    {
        $path = $uploaded->store('files', $this->disk);

        $file =  StorageFile::create([
            'uploader_id' => $user->id,
            'filename' => pathinfo($uploaded->getClientOriginalName(), PATHINFO_FILENAME),
            'real_filename' => pathinfo($uploaded->getClientOriginalName(), PATHINFO_FILENAME),
            'extension' => $uploaded->getClientOriginalExtension(),
            'mime_type' => $uploaded->getClientMimeType(),
            'disk' => $this->disk,
            'disk_path' => $path,
        ]);

        return $file;
    }
}
