<?php

namespace Tests\Feature\Api;

use App\Models\Post;
use App\Models\User;
use Tests\SeedsPermissions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostTest extends TestCase
{
    use RefreshDatabase, SeedsPermissions;

    public function testGuestCanViewAll()
    {
        $this->json('get', '/api/posts')->assertOk();
        $post = Post::factory()->create();

        $this->json('get', '/api/posts/' . $post->slug)->assertOk();
    }

    public function testUnauthorized()
    {
        $this->json('post', '/api/posts')->assertUnauthorized();

        $post = Post::factory()->create();

        $this->json('put', '/api/posts/' . $post->slug)->assertUnauthorized();
        $this->json('delete', '/api/posts/' . $post->slug)->assertUnauthorized();
    }

    public function testContentManagerCanNotStoreWithoutData()
    {
        $user = $this->createActor()->givePermissionTo('create any posts');

        $response = $this->actingAs($user, 'api')->json('post', '/api/posts');

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['title', 'content', 'slug']);
    }

    public function testContentManagerCanNotStoreExistingSlug()
    {
        $user = $this->createActor()->givePermissionTo('create any posts');

        $post = Post::factory()->create([
            'slug' => 'first'
        ]);

        $response = $this->actingAs($user, 'api')->json('post', '/api/posts', [
            'slug' => 'first'
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['slug']);
    }

    public function testContentManagerCanStore()
    {
        $user = $this->createActor()->givePermissionTo('create any posts');

        $response = $this->actingAs($user, 'api')->json('post', '/api/posts', [
            'title' => 'Touché',
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'slug' => 'la-slug',
        ]);

        /** @var \App\Models\Post $post */
        $post = Post::query()->find($response->json('data.id'));

        $response->assertStatus(201);

        $this->assertTrue($post->status === Post::STATUS_PUBLISHED);
    }

    public function testContentManagerCanDestroy()
    {
        $post = Post::factory()->create();
        $user = $this->createActor()->givePermissionTo('delete any posts');

        $response = $this->actingAs($user, 'api')
            ->json('delete', sprintf('/api/posts/%s', $post->slug));

        $response->assertStatus(204);

        $this->assertSoftDeleted('posts', ['id' => $post->id]);

        // assert not published
        $response = $this->json('get', sprintf('/api/posts/%s', $post->slug));
        $response->assertNotFound();
    }

}
