<?php

namespace Tests\Feature\Api\Settings;

use Tests\CreatesActor;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;

class ChangePasswordTest extends TestCase
{
    use RefreshDatabase, CreatesActor;

    protected function changePasswordUrl()
    {
        return '/api/settings/change-password';
    }

    public function testGuestCanNotChangePassword()
    {
        $this->json('post', $this->changePasswordUrl())->assertUnauthorized();
    }

    public function testUserCanNotChangePasswordWithoutData()
    {
        $this->createActor();

        $this->json('post', $this->changePasswordUrl())
            ->assertStatus(422)
            ->assertJsonValidationErrors(['old_password', 'password']);
    }

    public function testUserCanNotChangePasswordWithWrongOldPassword()
    {
        $this->createActor([
            'password' => Hash::make($oldPassword = 'reported')
        ]);

        $this->json('post', $this->changePasswordUrl(), [
            'old_password' => 'glhff',
            'password' => $newPassword = 'git_gud',
        ])->assertStatus(422)->assertJsonValidationErrors(['old_password']);
    }

    public function testUserCanNotChangePasswordWithoutConfirmation()
    {
        $this->createActor([
            'password' => Hash::make($oldPassword = 'reported')
        ]);

        $this->json('post', $this->changePasswordUrl(), [
            'old_password' => $oldPassword,
            'password' => $newPassword = 'git_gud',
        ])->assertStatus(422)->assertJsonValidationErrors(['password']);
    }

    public function testUserCanChangePassword()
    {
        $user = $this->createActor([
            'password' => Hash::make($oldPassword = 'reported')
        ]);

        $this->json('post', $this->changePasswordUrl(), [
            'old_password' => $oldPassword,
            'password' => $newPassword = 'git_gud',
            'password_confirmation' => $newPassword
        ])->assertNoContent();

        $user->refresh();

        $this->assertTrue(Hash::check($newPassword, $user->password));
    }
}
