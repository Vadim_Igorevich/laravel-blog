<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Role;
use App\Events\UserCreated;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Tests\SeedsPermissions;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase, SeedsPermissions;

    public function testGuestCanNotViewList()
    {
        $this->json('get', '/api/users')->assertUnauthorized();
    }

    public function testUserWithoutPermissionCanNotViewList()
    {
        $this->createActor();

        $this->json('get', '/api/users')->assertForbidden();
    }

    public function testUserWithPermissionCanViewList()
    {
        $this->createActor()->givePermissionTo('view any users');

        User::factory(10)->create();

        $this->json('get', '/api/users')
            ->assertOk()
            ->assertJsonCount(11, 'data');
    }

    public function testGuestCanNotStore()
    {
        $this->json('post', '/api/users')->assertUnauthorized();
    }

    public function testUserWithoutPermissionCanNotStore()
    {
        $this->createActor();
        $this->json('post', '/api/users')->assertForbidden();
    }

    public function testUserCanNotStoreWithoutData()
    {
        $this->createActor()->givePermissionTo('create any users');

        $this->json('post', '/api/users')
            ->assertStatus(422)
            ->assertJsonValidationErrors(['name', 'email']);
    }

    public function testUserCanNotStoreIfEmailAlreadyExists()
    {
        $this->createActor([
            'email' => 'wrecking@ball.com'
        ])->givePermissionTo('create any users');

        $this->json('post', '/api/users', [
            'name' => 'Wrecking Ball Again',
            'email' => 'wrecking@ball.com'
        ])
            ->assertStatus(422)
            ->assertJsonValidationErrors(['email']);
    }

    public function testUserCanStoreWithoutPassword()
    {
        Event::fake();

        $this->createActor()->givePermissionTo('create any users');

        $this->json('post', '/api/users', [
            'name' => 'Road Hog',
            'email' => 'road@hog.com',
        ])->assertStatus(201);

        Event::assertDispatched(UserCreated::class);
    }

    public function testGuestCanNotView()
    {
        $user = $this->createUser();
        $this->json('get', '/api/users/' . $user->id)->assertUnauthorized();
    }

    public function testUserWithoutPermissionCanNotView()
    {
        $user = $this->createActor();
        $this->json('get', '/api/users/' . $user->id)->assertForbidden();
    }

    public function testUserCanView()
    {
        $user = $this->createActor()->givePermissionTo('view any users');

        $this->json('get', '/api/users/' . $user->id)
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    'id', 'name', 'email', 'created_at', 'updated_at', 'avatar_url', 'roles'
                ]
            ]);
    }

    public function testGuestCanNotUpdate()
    {
        $user = $this->createUser();
        $this->json('put', '/api/users/' . $user->id)->assertUnauthorized();
    }

    public function testUserWithoutPermissionCanNotUpdate()
    {
        $user = $this->createActor();
        $this->json('put', '/api/users/' . $user->id)->assertForbidden();
    }

    public function testUserCanUpdateName()
    {
        $user = $this->createActor([
            'name' => 'D.VA',
            'email' => 'pedro@valakas.com'
        ])->givePermissionTo('update any users');

        $this->json('put', '/api/users/' . $user->id, [
            'name' => 'Pedro'
        ])->assertOk();

        $user->refresh();

        $this->assertEquals($user->name, 'Pedro');
        $this->assertEquals($user->email, 'pedro@valakas.com'); // email not updated
    }

    public function testUserCanUpdateEmail()
    {
        $user = $this->createActor([
            'name' => 'D.VA',
            'email' => 'pedro@valakas.com'
        ])->givePermissionTo('update any users');

        $this->json('put', '/api/users/' . $user->id, [
            'email' => 'd.va@overwarch.com'
        ])->assertOk();

        $user->refresh();

        $this->assertEquals($user->name,  'D.VA'); // name not updated
        $this->assertEquals($user->email, 'd.va@overwarch.com');
    }

    public function testUserCanNotUpdateToExistingEmail()
    {
        $user = $this->createActor([
            'email' => 'reinhardt@overwarch.com'
        ])->givePermissionTo('update any users');

        $model = $this->createUser([
            'email' => 'd.va@overwarch.com'
        ]);

        $this->json('put', '/api/users/' . $model->id, [
            'email' => 'reinhardt@overwarch.com'
        ])->assertJsonValidationErrors(['email']);

        $user->refresh();

        $this->assertEquals($model->email, 'd.va@overwarch.com'); // not updated
    }

    public function testUserCanUpdatePassword()
    {
        $user = $this->createActor()->givePermissionTo('update any users');

        $this->json('put', '/api/users/' . $user->id, [
            'password' => 'sound-barrier'
        ])->assertOk();

        $user->refresh();

        $this->assertTrue(Hash::check('sound-barrier', $user->password));
    }

    public function testUserCanAssignRoles()
    {
        $user = $this->createActor()->givePermissionTo('update any users');

        Role::findOrCreate('tank', 'web');
        Role::findOrCreate('healer', 'web');

        $this->assertCount(0, $user->roles);

        $this->json('put', '/api/users/' . $user->id, [
            'roles' => [
                ['name' => 'tank'],
                ['name' => 'healer'],
            ]
        ])->assertOk();

        $user->refresh();
        $this->assertCount(2, $user->roles);
    }

    public function testUserCanRemoveRoles()
    {
        $user = $this->createActor()->givePermissionTo('update any users');

        Role::findOrCreate('tank', 'web');
        Role::findOrCreate('healer', 'web');

        $user->assignRole(['tank', 'healer']);

        $this->assertCount(2, $user->roles);

        $this->json('put', '/api/users/' . $user->id, [
            'roles' => [
                ['name' => 'tank'],
            ]
        ])->assertOk();

        $user->refresh();
        $this->assertCount(1, $user->roles); // no more healer role
    }

    public function testGuestCanNotDelete()
    {
        $user = $this->createUser();
        $this->json('delete', '/api/users/' . $user->id)->assertUnauthorized();
    }

    public function testUserWithoutPermissionCanNotDelete()
    {
        $user = $this->createActor();
        $this->json('delete', '/api/users/' . $user->id)->assertForbidden();
    }

    public function testUserCanDelete()
    {
        $user = $this->createActor()->givePermissionTo('delete any users');

        $this->json('delete', '/api/users/' . $user->id)->assertNoContent();

        $this->assertSoftDeleted('users', ['id' => $user->id]);
    }
}
