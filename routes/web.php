<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => true]);

Route::middleware('auth:web')->group(function () {
    Route::get('{view}', [\App\Http\Controllers\RouterController::class, 'show'])
        ->where('view', '(.*)')
        ->name('router');
});
