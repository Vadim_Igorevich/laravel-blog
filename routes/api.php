<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'App\Http\Controllers\Api', 'middleware' => 'throttle:120,1'], function () {
    /*
    |--------------------------------------------------------------------------
    | Public Routes
    |--------------------------------------------------------------------------
    */
    // Posts
    Route::apiResource('posts', 'PostController')->only('index', 'show');

    Route::get('storage/files/{file}/download', 'StorageFileController@download')->name('storage.files.download');
    Route::get('storage/files/{file}/display', 'StorageFileController@display')->name('storage.files.display');
    /*
    |--------------------------------------------------------------------------
    | Auth Routes
    |--------------------------------------------------------------------------
    */
    Route::middleware('auth:api')->group(function () {
        Route::get('/user', function (Request $request) {
            return $request->user();
        });

        Route::apiResource('users', 'UserController');
        Route::prefix('settings')->namespace('Settings')->as('settings.')->group(function () {
            Route::post('change-password', 'ChangePasswordController@changePassword')->name('changePassword');
        });

        // Posts
        Route::apiResource('posts', 'PostController')->except('index', 'show');

        // StorageFile
        Route::put('storage/files/{file}', 'StorageFileController@update');
        Route::post('storage/files', 'StorageFileController@upload')->name('storage.files.upload');
        Route::get('storage/files/{file}', 'StorageFileController@meta')->name('storage.files.meta');
        Route::delete('storage/files/{file}', 'StorageFileController@unlink')->name('storage.files.unlink');
    });
});


