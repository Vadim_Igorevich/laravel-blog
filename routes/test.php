<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Broadcast::routes(['middleware' => ['auth:api']]);

Route::group(['namespace' => 'Api', 'as' => 'api.', 'middleware' => 'throttle:120,1'], function () {

    /*
    |--------------------------------------------------------------------------
    | Public Routes
    |--------------------------------------------------------------------------
    |
    | These actions are not authorized. This means you SHOULD NOT use authorization checks like gates and policies
    | for routes below.
    |
    */

    // Areas
    Route::apiResource('areas', 'AreaController')->only(['index']);

    // Towns
    Route::apiResource('towns', 'TownController')->only(['index']);

    // DealReview
    Route::apiResource('deals.review', 'DealReviewController')->only(['store']);
//    Route::apiResource('chats.review', 'ChatReviewController')->only(['store']);

    /*
    |--------------------------------------------------------------------------
    | Guest Routes
    |--------------------------------------------------------------------------
    |
    | Only for unauthenticated users.
    |
    */

    Route::group(['middleware' => ['guest:api']], function () {

        // Auth
        Route::post('authenticate', 'AuthController@authenticate')->name('authenticate');

        // Registers
        Route::post('register', 'RegisterController@register')->name('register');

    });


    /*
    |--------------------------------------------------------------------------
    | Private Routes
    |--------------------------------------------------------------------------
    |
    | Protected as fuck.
    |
    */
    Route::group(['middleware' => ['auth:api']], function () {

        Route::get('counters', 'NotificationController@counters')->name('notifications.counters');

        // Users
        Route::post('users/{user}/phones/{phone}/remote-call', 'UserPhoneController@remoteCall')->name('users.phones.remoteCall');
        Route::apiResource('users', 'UserController');

        // Posts
        Route::apiResource('posts', 'PostController');

        // Areas
        Route::apiResource('areas', 'AreaController')->only(['update']);

        // Documents
        Route::apiResource('documents', 'DocumentController');

        // Invoices
        Route::post('invoices/bulk', 'InvoiceController@bulk')->name('invoices.bulk');
        Route::post('invoices/{invoice}/consider', 'InvoiceController@consider')->name('invoices.consider');
        Route::post('invoices/{invoice}/approve', 'InvoiceController@approve')->name('invoices.approve');
        Route::post('invoices/{invoice}/buhapprove', 'InvoiceController@buhapprove')->name('invoices.buhapprove');
        Route::post('invoices/{invoice}/decline', 'InvoiceController@decline')->name('invoices.decline');
        Route::post('invoices/{invoice}/revise', 'InvoiceController@revise')->name('invoices.revise');
        Route::post('invoices/{invoice}/sent', 'InvoiceController@sent')->name('invoices.sent');
        Route::post('invoices/{invoice}/pay', 'InvoiceController@pay')->name('invoices.pay');
        Route::post('invoices/{invoice}/to-verify', 'InvoiceController@toVerify')->name('invoices.toVerify');
        Route::post('invoices/{invoice}/verify', 'InvoiceController@verify')->name('invoices.verify');
        Route::apiResource('invoices', 'InvoiceController')->only(['index', 'store', 'show', 'update', 'destroy']);

        // InvoiceStorageFiles
        Route::post('invoices/{invoice}/files/{file}', 'InvoiceStorageFileController@attach')
            ->name('invoices.files.attach');
        Route::delete('invoices/{invoice}/files/{file}', 'InvoiceStorageFileController@detach')
            ->name('invoices.files.detach');

        // ReclamationAttachmentController
        Route::post('reclamations/{reclamation}/attachments/{file}', 'ReclamationAttachmentController@attach')
            ->name('reclamations.attachments.attach');
        Route::delete('reclamations/{reclamation}/attachments/{file}', 'ReclamationAttachmentController@detach')
            ->name('reclamations.attachments.detach');

        Route::post('reclamations/{reclamation}/attachments/{file}', 'ReclamationAttachmentController@attach')
            ->name('reclamations.attachments.attach');
        Route::delete('reclamations/{reclamation}/attachments/{file}', 'ReclamationAttachmentController@detach')
            ->name('reclamations.attachments.detach');

        Route::post('reclamations/{reclamation}/documents/{file}', 'ReclamationDocumentController@attach')
            ->name('reclamations.documents.attach');
        Route::delete('reclamations/{reclamation}/documents/{file}', 'ReclamationDocumentController@detach')
            ->name('reclamations.documents.detach');

        // Shipments
        Route::post('shipments/{shipment}/receive', 'ShipmentController@receive')->name('shipments.receive');
        Route::apiResource('shipments', 'ShipmentController')->only(['index', 'show', 'update']);

        // InvoiceCostItems
        Route::apiResource('invoice-cost-items', 'InvoiceCostItemController');

        // Transports
        Route::apiResource('transports', 'TransportController')->only(['index', 'show']);
        Route::apiResource('user-motivations', 'UserMotivationController')->only(['index','update']);
        Route::apiResource('year-motivations', 'YearMotivationController')->only(['index','update']);

        // Delivery Methods
        Route::apiResource('delivery-methods', 'DeliveryMethodController')->only(['index', 'show']);

        // StorageFile
        Route::post('storage/files', 'StorageFileController@upload')->name('storage.files.upload');
        Route::get('storage/files/{file}', 'StorageFileController@meta')->name('storage.files.meta');
        Route::delete('storage/files/{file}', 'StorageFileController@unlink')->name('storage.files.unlink');
        Route::get('storage/files/{file}/download', 'StorageFileController@download')->name('storage.files.download');
        Route::get('storage/files/{file}/display', 'StorageFileController@display')->name('storage.files.display');

        // Suppliers
        Route::apiResource('suppliers', 'SupplierController')->only(['index', 'store']);

        // Orders
        Route::apiResource('orders', 'OrderController')->only(['index']);
        Route::get('orders/order-info', 'OrderController@orderInfo')->name('orders.order-info');

        // Dealers
        Route::apiResource('dealers', 'DealerController')->only(['index']);

        // DealerReports
        Route::apiResource('dealer-reports', 'DealerReportController')->only(['index', 'show', 'store']);
        Route::post('dealer-reports/{dealer_report}/callback', 'DealerReportController@callback')->name('dealer-reports.callback');
        Route::post('dealer-reports/{dealer_report}/order', 'DealerReportController@order')->name('dealer-reports.order');
        Route::post('dealer-reports/{dealer_report}/reject', 'DealerReportController@reject')->name('dealer-reports.reject');

        // VisitReports
        Route::get('visit-reports/download', 'VisitReportController@download')->name('visit-reports.download');
        Route::apiResource('visit-reports', 'VisitReportController')->only(['index', 'show', 'store']);

        // VisitWorkTypes
        Route::apiResource('visit-work-types', 'VisitWorkTypeController')->only(['index']);

        // Fitters
        Route::get('firms/export', 'FirmController@export');
        Route::apiResource('firms', 'FirmController');

        Route::apiResource('fitters', 'FitterController')->only(['index', 'show']);

        // Workers
        Route::apiResource('workers', 'WorkerController')->only(['index']);

        // Workers
        Route::apiResource('organizations', 'OrganizationController')->only(['index']);
        Route::get('organizations/reclamation-organizations', 'OrganizationController@reclamationOrganizations')->name('organizations.reclamationOrganizations');
        //Route::get('mailbox/attachments/{attachment}', 'MailboxController@attachment')->name('messages.attachment');

        // Roles
        Route::apiResource('roles', 'RoleController')->only(['index', 'show']);

        // Companies
        Route::apiResource('companies', 'CompanyController')->only(['index', 'store', 'update', 'show']);

        Route::get('purveyors/active', 'PurveyorController@active')->name('purveyors.active');
        Route::apiResource('purveyors', 'PurveyorController')->only(['index', 'store', 'update', 'show']);
        Route::post('purveyors/{purveyor}/copy', 'PurveyorController@copy')->name('purveyors.copy');
        Route::post('purveyors/{purveyor}/process', 'PurveyorController@process')->name('purveyors.process');
        Route::post('purveyors/{purveyor}/to-reprocess', 'PurveyorController@toReprocess')->name('purveyors.toReprocess');
        Route::post('purveyors/{purveyor}/approve', 'PurveyorController@approve')->name('purveyors.approve');
        Route::post('purveyors/{purveyor}/regional-approve', 'PurveyorController@regionalApprove')->name('purveyors.regionalApprove');
        Route::post('purveyors/{purveyor}/to-approve', 'PurveyorController@toApprove')->name('purveyors.toApprove');
        Route::post('purveyors/{purveyor}/to-regional-approve', 'PurveyorController@toRegionalApprove')->name('purveyors.toRegionalApprove');


        Route::get('monthly-plans/{monthly_plan}/info', 'MonthlyPlanController@info')->name('monthly-plans.info');
        Route::post('monthly-plans/{monthly_plan}/approve', 'MonthlyPlanController@approve')->name('monthly-plans.approve');
        Route::post('monthly-plans/{monthly_plan}/process', 'MonthlyPlanController@process')->name('monthly-plans.process');
        Route::post('monthly-plans/{monthly_plan}/to-reprocess', 'MonthlyPlanController@toReprocess')->name('monthly-plans.toReprocess');
        Route::get('monthly-plans/regional-sales', 'MonthlyPlanController@regionalSales')->name('monthly-plans.regionalSales');
        Route::apiResource('monthly-plans', 'MonthlyPlanController')->only(['index', 'store', 'update', 'show']);


        Route::get('engineer-calculations/list', 'EngineerCalculationController@list')->name('engineer-calculations.list');
        Route::post('engineer-calculations/adjacency-attributes', 'EngineerCalculationController@adjacencyAttributes')->name('engineer-calculations.adjacencyAttributes');
        Route::apiResource('engineer-calculations', 'EngineerCalculationController')->only(['index', 'store', 'update', 'show']);
        Route::get('engineer-calculations/{engineer_calculation}/availiable-attributes', 'EngineerCalculationController@availiableAttributes')->name('engineer-calculations.availiableAttributes');

        Route::get('engineer-calculations/{engineer_calculation}/availiable-profile-systems', 'EngineerCalculationController@availiableProfileSystems')->name('engineer-calculations.availiableProfileSystems');
        Route::get('engineer-calculations/{engineer_calculation}/child-calculations', 'EngineerCalculationController@childCalculations')->name('engineer-calculations.childCalculations');

        Route::post('engineer-calculations/{engineer_calculation}/add-profile-systems', 'EngineerCalculationController@addProfileSystems')->name('engineer-calculations.addProfileSystems');
        Route::post('engineer-calculations/{engineer_calculation}/save-manager-comment', 'EngineerCalculationController@saveManagerComment')->name('engineer-calculations.saveManagerComment');
        Route::post('engineer-calculations/{engineer_calculation}/add-attributes', 'EngineerCalculationController@addAttributes')->name('engineer-calculations.addAttributes');
        Route::get('engineer-calculations/{engineer_calculation}/priority', 'EngineerCalculationController@priority')->name('engineer-calculations.priority');
        Route::get('engineer-calculations/{engineer_calculation}/document', 'EngineerCalculationController@document')->name('engineer-calculations.document');
        Route::post('engineer-calculations/{engineer_calculation}/recount', 'EngineerCalculationController@recount')->name('engineer-calculations.recount');
        Route::post('engineer-calculations/{engineer_calculation}/launch', 'EngineerCalculationController@launch')->name('engineer-calculations.launch');
        Route::post('engineer-calculations/{engineer_calculation}/save-files', 'EngineerCalculationController@saveFiles')->name('engineer-calculations.saveFiles');
        //Route::post('deals/{deal}/gager-processed', 'DealController@gagerProcessed')->name('deals.gagerProcessed');
//		Route::post('dealer-reports/{dealer_report}/order', 'DealerReportController@order')->name('dealer-reports.order');

        // DealerReports
//      Route::apiResource('dealer-reports', 'DealerReportController')->only(['index', 'show', 'store']);
//      Route::post('dealer-reports/{dealer_report}/callback', 'DealerReportController@callback')->name('dealer-reports.callback');
//      Route::post('dealer-reports/{dealer_report}/order', 'DealerReportController@order')->name('dealer-reports.order');
//      Route::post('dealer-reports/{dealer_report}/reject', 'DealerReportController@reject')->name('dealer-reports.reject');



        // RegionalPlans
        Route::get('regional-plans', 'RegionalPlanController@index')->name('regional-plans.index');
        Route::post('regional-plans', 'RegionalPlanController@make')->name('regional-plans.make');

        // Chats
//        Route::post('chats/{chat}/review', 'ChatController@review')->name('chats.review');
        Route::post('chats/{chat}/join', 'ChatController@join')->name('chats.join');
        Route::post('chats/{chat}/leave', 'ChatController@leave')->name('chats.leave');
        Route::post('chats/{chat}/archive', 'ChatController@archive')->name('chats.archive');
        Route::post('chats/skip-reviews', 'ChatController@skipReviews');
        Route::get('chats/check-reviews', 'ChatController@checkReviews');
        Route::apiResource('chats', 'ChatController')->only(['index', 'show', 'store']);
        Route::apiResource('chats.messages', 'ChatMessageController')->only(['index', 'show', 'store']);

        // Registers
        Route::post('registers/{register}/approve', 'RegisterController@approve')->name('registers.approve');
        Route::post('registers/{register}/decline', 'RegisterController@decline')->name('registers.decline');
        Route::apiResource('registers', 'RegisterController')->only(['index', 'show']);

        // Calculations
        Route::post('calculations/{calculation}/take', 'CalculationController@take')->name('calculations.take');
        Route::post('calculations/{calculation}/process', 'CalculationController@process')->name('calculations.process');
        Route::post('calculations/{calculation}/accept', 'CalculationController@accept')->name('calculations.accept');
        Route::post('calculations/{calculation}/decline', 'CalculationController@decline')->name('calculations.decline');
        Route::post('calculations/{calculation}/close', 'CalculationController@close')->name('calculations.close');
        Route::apiResource('calculations', 'CalculationController')->except(['update', 'destroy']);

        // Reclamations
        Route::post('reclamations/{reclamation}/take', 'ReclamationController@take')->name('reclamations.take');
        Route::post('reclamations/{reclamation}/process', 'ReclamationController@process')->name('reclamations.process');
        Route::post('reclamations/{reclamation}/to-reprocess', 'ReclamationController@toReprocess')->name('reclamations.toReprocess');
        Route::post('reclamations/{reclamation}/update', 'ReclamationController@update')->name('reclamations.update');
        Route::post('reclamations/{reclamation}/accept', 'ReclamationController@accept')->name('reclamations.accept');
        Route::post('reclamations/{reclamation}/resolve', 'ReclamationController@resolve')->name('reclamations.resolve');
        Route::post('reclamations/{reclamation}/decline', 'ReclamationController@decline')->name('reclamations.decline');
        Route::post('reclamations/{reclamation}/approve', 'ReclamationController@approve')->name('reclamations.approve');
        Route::post('reclamations/{reclamation}/decommissioned', 'ReclamationController@decommissioned')->name('reclamations.decommissioned');
        Route::post('reclamations/{reclamation}/close', 'ReclamationController@close')->name('reclamations.close');
        Route::get('reclamations/{reclamation}/document', 'ReclamationController@document')->name('reclamations.document');
        Route::get('reclamations/{reclamation}/download', 'ReclamationController@download')->name('reclamations.download');
        Route::apiResource('reclamations', 'ReclamationController')->except(['destroy']);

        Route::apiResource('reclamation-reports', 'ReclamationReportController')->except(['update', 'destroy']);

        Route::apiResource('settings', 'SettingController')->except(['update', 'destroy']);
        Route::post('settings/update-settings', 'SettingController@updateSettings')->name('settings.updateSettings');

        // Mailbox
        Route::get('mailbox/attachments/{attachment}', 'MailboxController@attachment')->name('messages.attachment');
        Route::post('mailbox/messages/bulk', 'MailboxController@bulk')->name('messages.bulk');
        Route::post('mailbox/messages/compose', 'MailboxController@compose')->name('messages.compose');
        Route::post('mailbox/messages/{message}/reply', 'MailboxController@reply')->name('messages.reply');
        Route::apiResource('mailbox/messages', 'MailboxController')->except(['update', 'destroy']);

        // Identity
        Route::get('identity', 'IdentityController@show');

        // Statistics
        Route::get('statistics/calculations', 'Statistics\CalculationController@index');
        Route::get('statistics/calculations/chart-data', 'Statistics\CalculationController@chartData');

        Route::get('statistics/calculations/export', 'Statistics\CalculationController@export');
        Route::get('statistics/review', 'Statistics\ReviewController@index');
        Route::get('statistics/retail', 'Statistics\RetailController@index');
        Route::get('statistics/retail/export', 'Statistics\RetailController@export');
        Route::get('statistics/reclamations', 'Statistics\ReclamationController@index');
        Route::get('statistics/reclamations/export', 'Statistics\ReclamationController@export');
        Route::get('statistics/dealer-reports', 'Statistics\DealerReportController@index');
        Route::get('statistics/dealer-reports/export', 'Statistics\DealerReportController@export');
        Route::get('statistics/engineer-calculations', 'Statistics\EngineerCalculationController@index');
        Route::get('statistics/engineer-calculations/export', 'Statistics\EngineerCalculationController@export');

        // Device push token
        Route::post('push-token', 'PushTokenController@attach')->name('push-token.attach');
        Route::delete('push-token', 'PushTokenController@detach')->name('push-token.detach');

        // Retail
        // Customers
        Route::apiResource('customers', 'CustomerController')->only(['index', 'store', 'update', 'show']);
        Route::apiResource('departments', 'DepartmentController')->only(['index', 'store', 'update', 'show']);
        Route::apiResource('units', 'UnitController')->only(['index', 'store', 'update', 'show']);
        Route::get('departments/{department}/available-members', 'DepartmentController@availableMembers')->name('departments.availableMembers');
        Route::post('departments/{department}/add-members', 'DepartmentController@addMembers')->name('departments.addMembers');
        Route::delete('departments/{department}/delete-member/{member}', 'DepartmentController@deleteMember')->name('departments.departments.deleteMember');

        // Deals
        Route::apiResource('deals', 'DealController')->only(['index', 'store', 'show']);
        Route::post('deals/{deal}/pick-deal', 'DealController@pickDeal')->name('deals.pickDeal');//TODO
        Route::post('deals/{deal}/callback', 'DealController@callback')->name('deals.callback');
        Route::post('deals/{deal}/calculate', 'DealController@calculate')->name('deals.calculate');
        Route::post('deals/{deal}/measure', 'DealController@measure')->name('deals.measure');
        Route::post('deals/{deal}/order', 'DealController@order')->name('deals.order');
        Route::post('deals/{deal}/complete', 'DealController@complete')->name('deals.complete');
        Route::post('deals/{deal}/reject', 'DealController@reject')->name('deals.reject');
        Route::post('deals/{deal}/without-metering', 'DealController@withoutMetering')->name('deals.withoutMetering');
        Route::post('deals/{deal}/need-metering', 'DealController@needMetering')->name('deals.needMetering');
        Route::post('deals/{deal}/wait-metering', 'DealController@waitMetering')->name('deals.waitMetering');
        Route::post('deals/{deal}/gager-processed', 'DealController@gagerProcessed')->name('deals.gagerProcessed');

        // Gagers
        Route::apiResource('gagers', 'GagerController')->only(['index', 'show']);

        // Event
        Route::post('events/{event}/resolve', 'EventController@resolve')->name('events.resolve');
        Route::apiResource('events', 'EventController')->only(['index', 'show']);

        // DealReview
        Route::apiResource('deals.review', 'DealReviewController')->only(['show']);
//        Route::apiResource('chats.review', 'ChatReviewController')->only(['show']);
        Route::apiResource('chats.review', 'ChatReviewController')->only(['store', 'show']);

        // DealComment TODO ?????????
        Route::apiResource('deals.comments', 'DealCommentController')->only(['index','store','show']);
        Route::apiResource('dealer-reports.comments', 'DealerReportCommentController')->only(['index','store','show']);

//		Route::get('dealer-reports/{dealer_report}/comments', 'DealerReportCommentController@order')->name('dealer-report-comments.index');

//		// DealerReports
//		Route::apiResource('dealer-reports/{dealer_report}/comments', 'DealerReportController')->only(['index', 'show', 'store']);
//		Route::post('dealer-reports/{dealer_report}/order', 'DealerReportController@order')->name('dealer-reports.order');
//		Route::post('dealer-reports/{dealer_report}/reject', 'DealerReportController@reject')->name('dealer-reports.reject');






        Route::group(['namespace' => 'Cabinet', 'as' => 'cabinet.'], function () {
            Route::get('cabinet/wincalc/download', 'WincalcController@download')->name('wincalc.download');
            Route::get('cabinet/dealer/balance', 'DealerController@balance')->name('dealer.balance');
            Route::get('cabinet/payments', 'PaymentController@index')->name('payments.index');
            Route::get('cabinet/orders', 'OrderController@index')->name('orders.index');
            Route::get('cabinet/orders/{uid}', 'OrderController@show')->name('orders.show');
            Route::get('cabinet/orders/{uid}/info', 'OrderController@info')->name('orders.info');
            Route::post('cabinet/orders/{uid}/pay-and-start', 'OrderController@payAndStart')->name('orders.payAndStart');
            Route::post('cabinet/orders/{uid}/cancel', 'OrderController@cancel')->name('orders.cancel');
        });

        // Telephony
        Route::get('telephony/receive-phone', 'TelephonyController@receivePhone')->name('telephony.receivePhone');
        Route::get('telephony/incoming', 'TelephonyController@incoming')->name('telephony.incoming');
        Route::get('telephony/add-phone-recording', 'TelephonyController@addPhoneRecording')->name('telephony.addPhoneRecording');
        Route::get('telephony/get-caller-name', 'TelephonyController@getCallerName')->name('telephony.getCallerName');
        Route::get('telephony/get-caller-name2', 'TelephonyController@getCallerName2')->name('telephony.getCallerName2');
        Route::get('telephony/ring', 'TelephonyController@ring')->name('telephony.ring');
        Route::get('telephony/stop_ring', 'TelephonyController@stop_ring')->name('telephony.stop_ring');
        Route::get('telephony/get_man_phone_list', 'TelephonyController@get_man_phone_list')->name('telephony.get_man_phone_list');
    });
});
