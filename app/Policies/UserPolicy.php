<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->can('view any users');
    }

    public function view(User $user, User $model)
    {
        return $user->can('view any users');
    }

    public function create(User $user)
    {
        return $user->can('create any users');
    }

    public function update(User $user, User $model)
    {
        return $user->can('update any users');
    }

    public function delete(User $user, User $model)
    {
        return $user->can('delete any users');
    }

    public function restore(User $user, User $model)
    {
        //
    }

    public function forceDelete(User $user, User $model)
    {
        //
    }
}
