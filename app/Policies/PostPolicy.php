<?php

namespace App\Policies;

use App\Models\Post;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->can('view any posts');
    }

    public function view(User $user, Post $post)
    {
        return $user->can('view any posts');
    }

    public function create(User $user)
    {
        return $user->can('create any posts');
    }

    public function update(User $user, Post $post)
    {
        return $user->can('update any posts');
    }

    public function delete(User $user, Post $post)
    {
        return $user->can('delete any posts');
    }

    public function restore(User $user, Post $post)
    {
        //
    }

    public function forceDelete(User $user, Post $post)
    {
        //
    }
}
