<?php

namespace App\View\Composers;

use App\Http\Resources\Role as RoleResource;
use App\Models\Role;
use Illuminate\View\View;

final class RouterComposer
{
    public function compose(View $view)
    {
        $view->with('config', [
            'users' => [
                'roles' => RoleResource::collection(
                    Role::all()
                ),
//                'roles' => [
//                    [
//                        'key' => User::ROLE_ADMIN,
//                        'label' => 'Администратор'
//                    ],
//                    [
//                        'key' => User::ROLE_MANAGER,
//                        'label' => 'Менеджер'
//                    ],
//                    [
//                        'key' => User::ROLE_ACCOUNTANT,
//                        'label' => 'Бухгалтер'
//                    ],
//                ]
            ]
        ]);
    }
}
