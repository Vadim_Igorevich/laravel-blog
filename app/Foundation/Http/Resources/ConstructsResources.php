<?php

namespace App\Foundation\Http\Resources;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

trait ConstructsResources
{
    protected $includes = [];

    public function toResource(Model $model)
    {
        $model->load($this->includes());

        $className = $this->getResourcesNamespace() . '\\' . class_basename($model);

        // TODO fix exception raised from composer autoloader if class not exists
        if (class_exists($className)) {
            return new $className($model);
        }

        return new JsonResource($model);
    }

    public function toCollection(Builder $query)
    {
        $query = $query->with($this->includes());

        if (request()->query('page') === '-1') {
            $items = $query->get();
        } else {
            $items = $query->paginate();
        }

        $className = $this->getResourcesNamespace() . '\\' . class_basename($query->getModel());

        if (class_exists($className)) {
            return $className::collection($items);
        }

        return new ResourceCollection($items);
    }

    protected function getResourcesNamespace(): string
    {
        return trim(app()->getNamespace(), '\\') . '\Http\Resources';
    }

    protected function includes(): array
    {
        return $this->includes;
    }
}
