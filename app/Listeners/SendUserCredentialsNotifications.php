<?php

namespace App\Listeners;

use App\Events\UserCreated;
use App\Notifications\NewCredentials;

class SendUserCredentialsNotifications
{
    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        $event->user->notify(new NewCredentials($event->password));
    }
}
