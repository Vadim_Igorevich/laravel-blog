<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewCredentials extends Notification
{
    use Queueable;

    protected $password;

    /**
     * Create a new notification instance.
     *
     * @param string $password
     */
    public function __construct(string $password)
    {
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Ваши доступы в систему ' . config('app.name'))
            ->greeting('Приветствуем, ' . $notifiable->name . '!')
            ->line('Вы были зарегистрированы в системе ' . config('app.name') . ', вот ваши данные для входа:')
            ->line('**Логин:** ' . $notifiable->email)
            ->line('**Пароль:** ' . $this->password)
            ->action('Перейти на страницу входа', route('login'));
    }
}
