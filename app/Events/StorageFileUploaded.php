<?php

namespace App\Events;

use App\Models\StorageFile;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class StorageFileUploaded
{
    use Dispatchable, SerializesModels;

    public $file;

    /**
     * Create a new event instance.
     *
     * @param StorageFile $file
     */
    public function __construct(StorageFile $file)
    {
        $this->file = $file;
    }
}
