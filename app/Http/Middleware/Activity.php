<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class Activity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if ($user instanceof User) {
            $user->setAttribute('activity_at', $user->freshTimestamp());
            $user->save();
        }

        return $next($request);
    }
}
