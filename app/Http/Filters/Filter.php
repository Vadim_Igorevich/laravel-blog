<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

abstract class Filter
{
    public function handle($request, \Closure $next)
    {
        if (!request()->has($this->filterName())) {
            return $next($request);
        }

        $query = $next($request);

        return $this->apply($query);
    }

    protected function filterName(): string
    {
        return Str::snake(class_basename($this));
    }

    protected function filterValue()
    {
        return request($this->filterName());
    }

    abstract public function apply(Builder $query): Builder;

    protected function equals(Builder $query): Builder
    {
        return $query->where($this->filterName(), '=', $this->filterValue());
    }

    protected function like(Builder $query): Builder
    {
        return $query->where($this->filterName(), 'like', '%' . $this->filterValue() . '%');
    }
}
