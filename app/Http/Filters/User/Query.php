<?php

namespace App\Http\Filters\User;

use App\Http\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class Query extends Filter
{
    public function apply(Builder $query): Builder
    {
        return $query
            ->where('name', 'like', '%' . $this->filterValue() . '%');
    }
}
