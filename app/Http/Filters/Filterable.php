<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pipeline\Pipeline;

trait Filterable
{
    public static function filter(array $filters): Builder
    {
        return app(Pipeline::class)
            ->send(static::query())
            ->through($filters)
            ->thenReturn();
    }
}
