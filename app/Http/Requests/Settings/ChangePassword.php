<?php

namespace App\Http\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class ChangePassword extends FormRequest
{
    public function rules(): array
    {
        return [
            'old_password' => ['required', function ($attribute, $value, $fail) {
                if (!Hash::check($value, $this->user()->password)) {
                    $fail('Старый пароль не подходит.');
                }
            }],
            'password' => 'required|min:6|max:40|confirmed'
        ];
    }
}
