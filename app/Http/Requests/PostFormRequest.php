<?php

namespace App\Http\Requests;

use App\Models\Post;
use Illuminate\Validation\Rule;

class PostFormRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string', 'max:512'],
            'content' => ['required', 'string'],
            'description' => ['nullable', 'string'],
            'slug' => [
                'required',
                Rule::unique('posts')->ignore($this->route('post'), 'slug')
            ],
            'status' => ['nullable', Rule::in(Post::statuses())],
            'image_id' => ['nullable', 'exists:storage_files,id']
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'image_id' => 'Картинка',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'content' => 'Содержание',
            'slug' => 'ЧПУ',
            'status' => 'Статус',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Укажите заголовок.',
            'content.required' => 'Заполните содержимое',
            'slug.required' => 'Укажите ЧПУ.',
            'slug.unique' => 'Этот ЧПУ уже занят.',
        ];
    }
}
