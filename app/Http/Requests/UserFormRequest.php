<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserFormRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => [
                $this->isNewRecord() ? 'required' : 'sometimes',
                'string',
                'max:191'
            ],
            'email' => [
                $this->isNewRecord() ? 'required' : 'sometimes',
                'email',
                $this->uniqueRule()
            ],
            'password' => [
                'sometimes',
                'string',
                'min:6',
                'max:191'
            ],
            'roles' => ['array'],
            'roles.*.name' => ['required', 'exists:roles,name']
        ];
    }

    public function isNewRecord(): bool
    {
        return ! ($this->route('user') instanceof User);
    }

    protected function uniqueRule()
    {
        $unique = Rule::unique('users');
        if (!$this->isNewRecord()) {
            $unique->ignore($this->route('user')->id);
        }

        return $unique;
    }

    public function messages(): array
    {
        return [
            'email.unique' => 'Этот электронный адрес уже зарегистрирован.',
            'password.min' => 'Пароль должен быть длиннее :min символов.'
        ];
    }
}
