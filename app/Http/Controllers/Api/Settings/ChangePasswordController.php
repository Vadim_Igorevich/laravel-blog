<?php

namespace App\Http\Controllers\Api\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\Settings\ChangePassword;
use Illuminate\Support\Facades\Hash;

final class ChangePasswordController extends Controller
{
    public function changePassword(ChangePassword $request)
    {
        $request->user()->update([
            'password' => Hash::make($request->input('password'))
        ]);

        return response()->noContent();
    }
}
