<?php

namespace App\Http\Controllers\Api;

use App\Models\Post;
use App\Http\Requests\PostFormRequest;
use App\Http\Resources\Post as PostResource;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    protected $includes = ['author', 'image'];

    public function __construct()
    {
        $this->authorizeResource(Post::class, null, [
            'except' => [ 'index', 'show' ],
        ]);
    }

    public function index()
    {
        $query = Post::query()->latest();

        return $this->toCollection($query);
    }

    public function show(Post $post)
    {
        return $this->toResource($post);
    }

    public function store(PostFormRequest $request)
    {
        /** @var Post $post */
        $post = Post::query()->create([
            'author_id' =>  auth()->id(),
            'image_id' => $request->input('image_id'),
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'content' => $request->input('content'),
            'slug' => $request->input('slug'),
            'status' => $request->input('status', Post::STATUS_PUBLISHED),
        ]);

        return $this->toResource($post);
    }

    public function update(PostFormRequest $request, Post $post)
    {
        $post->update([
            'image_id' => $request->input('image_id'),
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'content' => $request->input('content'),
            'slug' => $request->input('slug'),
            'status' => $request->input('status', Post::STATUS_PUBLISHED),
        ]);

        return $this->toResource($post);
    }

    public function destroy(Post $post)
    {
        $post->delete();

        return response()->noContent();
    }
}
