<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Events\UserCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserFormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

final class UserController extends Controller
{
    protected $includes = ['roles', 'attachments'];

    public function __construct()
    {
        $this->authorizeResource(User::class);
    }

    public function index()
    {
        $query = User::query();

        return $this->toCollection($query);
    }

    public function store(UserFormRequest $request)
    {
        $user = User::query()->create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($password = $request->input('password', Str::random(8))),
            // TODO improve password randomizer (make password based on name and email)
        ]);

        $this->syncRoles($request, $user);

        event(new UserCreated($user, $password));

        return $this->toResource($user);
    }

    public function show(User $user)
    {
        return $this->toResource($user);
    }

    public function update(UserFormRequest $request, User $user)
    {
        $user->update([
            'name' => $request->input('name', $user->name),
            'email' => $request->input('email', $user->email),
            'password' => $request->has('password') ?
                Hash::make($request->input('password')) : $user->password,
        ]);

        $this->syncRoles($request, $user);

        return $this->toResource($user);
    }

    public function destroy(User $user)
    {
        $user->delete();

        return response()->noContent();
    }

    protected function syncRoles(UserFormRequest $request, User $user)
    {
        if ($request->has('roles')) {
            $user->syncRoles($request->input('roles.*.name'));
        }
    }
}
