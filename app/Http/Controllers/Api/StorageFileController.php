<?php

namespace App\Http\Controllers\Api;

use Str;
use Storage;
use App\Models\StorageFile;
use App\Events\StorageFileUploaded;
use App\Http\Controllers\Controller;
use App\Http\Requests\UploadStorageFileRequest;
use App\Http\Resources\StorageFileResource;
use Illuminate\Http\Request;

class StorageFileController extends Controller
{
    public $relationships = ['uploader'];

    protected $disk;

    public function __construct()
    {
        $this->disk = config('filesystems.default', 'local');

//        $this->middleware('can:upload,App\StorageFile')->only('upload');
//        $this->middleware('can:view,file')->only(['meta', 'download', 'display']);
//        $this->middleware('can:unlink,file')->only(['unlink']);
    }

    public function upload(UploadStorageFileRequest $request)
    {
        $uploaded = $request->file('file');

        $path = $uploaded->store('files', $this->disk);

        /** @var StorageFile $file */
        $file = StorageFile::query()->create([
            'uploader_id' => auth()->id(),
            'filename' => pathinfo($uploaded->getClientOriginalName(), PATHINFO_FILENAME),
            'real_filename' => pathinfo($uploaded->getClientOriginalName(), PATHINFO_FILENAME),
            'extension' => $uploaded->getClientOriginalExtension(),
            'mime_type' => $uploaded->getClientMimeType(),
            'disk' => $this->disk,
            'disk_path' => $path,
        ]);

        event(new StorageFileUploaded($file));

        return new StorageFileResource($file->load($this->relationships));
    }

    public function update(Request $request, StorageFile $file)
    {
        $file->update(['filename'=>$request->input('filename')]);

        return new StorageFileResource($file->load($this->relationships));
    }

    public function meta(StorageFile $file)
    {
        return new StorageFileResource($file->load($this->relationships));
    }

    public function download(StorageFile $file)
    {
        return $this->streamedResponse($file, 'attachment');
    }

    public function display(StorageFile $file)
    {
        return $this->streamedResponse($file, 'inline');
    }

    protected function streamedResponse(StorageFile $file, $disposition)
    {
        return Storage::disk($file->disk)->response($file->disk_path, $file->transliterated_file_name, [], $disposition);
    }

    public function unlink(StorageFile $file)
    {
        Storage::disk($file->disk)->delete($file->disk_path);

        $file->delete();

        return new StorageFileResource($file->load($this->relationships));
    }
}
