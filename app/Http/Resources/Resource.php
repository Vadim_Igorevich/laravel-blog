<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

abstract class Resource extends JsonResource
{
    use Serializes;
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'actions' => $request->user() instanceof User ? array_keys(array_filter($this->actions($request->user()))) : []
        ]);
    }

    public function actions(User $user): array
    {
        return [];
    }
}
