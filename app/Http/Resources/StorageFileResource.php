<?php

namespace App\Http\Resources;

use App\Http\Resources\User;
use App\Models\StorageFile;

/**
 * @mixin StorageFile
 */
class StorageFileResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'filename' => $this->filename,
            'real_filename' => $this->real_filename,
            'extension' => $this->extension,
            'mime_type' => $this->mime_type,
            'uploaded_at' => $this->serializeDate($this->uploaded_at),
            'uploader' => new User($this->whenLoaded('uploader')),
            'links' => $this->links($request)
        ];
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function links($request)
    {
        return [
            'display' => route('storage.files.display', ['file' => $this->id]),
            'download' => route('storage.files.download', ['file' => $this->id])
        ];
    }
}
