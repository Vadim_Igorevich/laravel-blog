<?php

namespace App\Http\Resources;

use App\Models\StorageFile;
use App\Models\User;

/**
 * @mixin \App\Models\Post
 */
class Post extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'image' => new StorageFileResource($this->whenLoaded('image')),
            'links' => $this->links($request)
        ]);
    }

    public function links($request)
    {
        return [
            'self' => route('posts.show', ['post' => $this->slug]),
        ];
    }

    public function actions(User $user): array
    {
        return [
            'view' => $user->can('view', $this->resource),
            'update' => $user->can('update', $this->resource),
            'delete' => $user->can('delete', $this->resource),
        ];
    }
}
