<?php

namespace App\Http\Resources;

class User extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'roles' => $this->roles->map(function ($role) {
                return [
                    'id' => $role->id,
                    'name' => $role->name,
                    'guard_name' => $role->guard_name,
                    'title' => $role->title,
                    'created_at' => $role->created_at,
                    'updated_at' => $role->updated_at,
                ];
            }),
            'attachments' => $this->attachments->map(function ($attachent) {
                return new StorageFileResource($attachent->file);
            })
        ]);
    }

    public function actions(\App\Models\User $user): array
    {
        return [
            'update' => $user->can('update', $this->resource),
            'delete' => $user->can('delete', $this->resource),
        ];
    }
}
