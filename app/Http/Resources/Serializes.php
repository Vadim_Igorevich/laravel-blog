<?php

namespace App\Http\Resources;

use Carbon\Carbon;

trait Serializes
{
    protected function serializeDate(?Carbon $carbon)
    {
        if ($carbon === null) {
            return null;
        }

        return $carbon->toDateTimeString();
    }
}
