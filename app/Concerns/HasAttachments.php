<?php

namespace App\Concerns;

use App\Models\Attachment;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Collection;

trait HasAttachments
{
    public function  attachments(): MorphMany
    {
        return $this->morphMany(Attachment::class, 'attachmentable');
    }

    public function extractAttachments($model, $request)
    {
        if (!$request->has('files')) {
            return;
        }

        $incoming = collect($request->input('files'));
        $model->attachments->each(function (Attachment $attachment) use ($incoming) {
            if (!$incoming->contains($attachment->storage_file_id )) {
                $attachment->delete();
            }
        });

        $attachments =  collect($request->input('files'));
        foreach ($attachments as $attachment){
            $issetAttachment = null;
            if($attachment){
                $issetAttachment = Attachment::query()->where('storage_file_id','=',$attachment)->first();
            }
            if(!$issetAttachment){
                $issetAttachment = $model->attachments()->firstOrCreate([
                    'storage_file_id' => $attachment,
                ]);
            }
            $issetAttachment->storage_file_id = $attachment;
            $issetAttachment->save();
        }
    }
}

