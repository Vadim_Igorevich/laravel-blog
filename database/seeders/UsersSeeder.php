<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory(50)->create()->each(function (User $user) {
            switch (mt_rand(1, 2)) {
                case 1:
                    $user->assignRole(User::ROLE_ADMIN);
                    break;
                case 2:
                    $user->assignRole(User::ROLE_MANAGER);
                    break;
            }
        });
    }
}
