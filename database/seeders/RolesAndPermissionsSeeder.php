<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Database\Seeder;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $this->call(PermissionsSeeder::class);

        $roles = [
            [
                'name' => 'admin',
                'title' => 'Администратор',
                'permissions' => '*',
            ],
            [
                'name' => 'manager',
                'title' => 'Менеджер',
                'permissions' => [
                    'view any users',
                    'update any users',
                    'view any posts',
                    'create any posts',
                    'update any posts',
                    'delete any posts'
                ]
            ]
        ];

        collect($roles)->each(function ($data) {
            if ($data['permissions'] === '*') {
                $data['permissions'] = Permission::all();
            }

            Role::findOrCreate($data['name'])
                ->givePermissionTo($data['permissions'])
                ->update([
                    'title' => $data['title']
                ]);
        });
    }
}
