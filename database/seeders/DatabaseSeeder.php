<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(UsersSeeder::class);

        User::factory()->create([
            'name' => 'Vadim',
            'email' => 'vadosrob@gmail.com',
            'password' => \Illuminate\Support\Facades\Hash::make('password'),
        ])->assignRole('admin');

        $this->call(PostsSeeder::class);
    }
}
