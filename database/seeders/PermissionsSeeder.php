<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $permissions = [
            'view any users',
            'create any users',
            'update any users',
            'delete any users',
            'view any posts',
            'create any posts',
            'update any posts',
            'delete any posts'
        ];

        collect($permissions)->each(function ($item) {
            Permission::findOrCreate($item);
        });
    }
}
