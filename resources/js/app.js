import Vue from 'vue'
import router from './router'
import store from './store'
import './components'
import './filters'
import './bootstrap'
import http from './util/axios'
import Funky from './Funky'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.prototype.$http = http
window.Funky = new Funky()

const app = new Vue({
    el: '#app',
    router,
    store,
    mounted: function () {
        window.Funky.$on('error', message => {
            this.$toasted.error(message, { icon: 'fe fe-alert-circle mr-2' })
        })

        window.Funky.$on('unauthorized', message => {
            this.$toasted.error(message, { icon: 'fe fe-alert-circle mr-2' })
            this.$router.push({ name: 'activity' })
        })

        window.Funky.$on('token-expired', () => {
            this.$toasted.show('Похоже, время вашей сессии истекло', {
                action: {
                    onClick: () => location.reload(),
                    text: 'Перезагрузить',
                },
                duration: null,
                type: 'error',
            })
        })
    },

    beforeDestroy () {
        window.Funky.$off('error')
        window.Funky.$off('unauthorized')
        window.Funky.$off('token-expired')
    }
});
