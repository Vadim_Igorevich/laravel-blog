import Vue from 'vue'
import moment from 'moment'

Vue.filter('formatDate', function (value) {
    return value ? moment(String(value)).format('DD.MM.YYYY') : ''
})

Vue.filter('formatDateTime', function (value) {
    return value ? moment(String(value)).format('DD.MM.YYYY HH:mm') : ''
})

// Vue.filter('formatCurrency', function (value, currency) {
//     const formatter = new Intl.NumberFormat('uk-UA', {
//         style: 'currency',
//         currency: currency
//     })
//     return formatter.format(value)
// })

Vue.filter('formatNumber', function (value) {
    if (value === null) {
        return null
    }

    return Number(parseFloat(value).toFixed(2)).toLocaleString('uk-UA',
        { minimumFractionDigits: 2
        })
})

Vue.filter('highlight', function (text, query) {
    if (!query) {
        return text
    }
    return text.replace(new RegExp(query.trim(), 'gi'), match => {
        return '<span class="highlight">' + match + '</span>'
    })
})
