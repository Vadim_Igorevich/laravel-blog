import Vue from 'vue'
import VueTheMask from 'vue-the-mask'
import VCalendar from 'v-calendar'
import Toasted from 'vue-toasted'
import router from '../router'
import Multiselect from 'vue-multiselect'
import VueConfirmDialog from 'vue-confirm-dialog'

Vue.use(VueTheMask)
Vue.use(VueConfirmDialog)

Vue.use(VCalendar, {
    firstDayOfWeek: 2,
    locale: 'ru'
})

Vue.use(Toasted, {
    router,
    theme: 'bubble',
    position: 'bottom-right',
    duration: 6000,
    iconPack: 'custom-class'
})

Vue.component('multiselect', Multiselect)
Vue.component('vue-confirm-dialog', VueConfirmDialog.default)
Vue.component('has-error', require('./HasError').default)
Vue.component('loading', require('./Loading').default)
Vue.component('pagination', require('./Pagination').default)
Vue.component('submit-button', require('./SubmitButton').default)
Vue.component('wrapper', require('./Wrapper').default)
Vue.component('storage-files', require('../components/StorageFiles').default)
Vue.component('storage-uploader', require('../components/StorageUploader').default)

