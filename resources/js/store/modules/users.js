export default {
    namespaced: true,

    state: () => ({
        roles: window.config && window.config.users && window.config.users.roles ? window.config.users.roles : []
    }),

    getters: {
        roles: state => state.roles
    },

    actions: {

    },

    mutations: {

    }
}