const transliterate = function (word) {
    let answer = ''
    let a = []

    a['Ё'] = 'YO'
    a['Й'] = 'I'
    a['Ц'] = 'TS'
    a['У'] = 'U'
    a['К'] = 'K'
    a['Е'] = 'E'
    a['Н'] = 'N'
    a['Г'] = 'G'
    a['Ш'] = 'SH'
    a['Щ'] = 'SCH'
    a['З'] = 'Z'
    a['Х'] = 'H'
    a['Ъ'] = '\''
    a['ё'] = 'yo'
    a['й'] = 'i'
    a['ц'] = 'ts'
    a['у'] = 'u'
    a['к'] = 'k'
    a['е'] = 'e'
    a['н'] = 'n'
    a['г'] = 'g'
    a['ш'] = 'sh'
    a['щ'] = 'sch'
    a['з'] = 'z'
    a['х'] = 'h'
    a['ъ'] = '\''
    a['Ф'] = 'F'
    a['Ы'] = 'I'
    a['В'] = 'V'
    a['А'] = 'a'
    a['П'] = 'P'
    a['Р'] = 'R'
    a['О'] = 'O'
    a['Л'] = 'L'
    a['Д'] = 'D'
    a['Ж'] = 'ZH'
    a['Э'] = 'E'
    a['ф'] = 'f'
    a['ы'] = 'i'
    a['в'] = 'v'
    a['а'] = 'a'
    a['п'] = 'p'
    a['р'] = 'r'
    a['о'] = 'o'
    a['л'] = 'l'
    a['д'] = 'd'
    a['ж'] = 'zh'
    a['э'] = 'e'
    a['Я'] = 'Ya'
    a['Ч'] = 'CH'
    a['С'] = 'S'
    a['М'] = 'M'
    a['И'] = 'I'
    a['Т'] = 'T'
    a['Ь'] = '\''
    a['Б'] = 'B'
    a['Ю'] = 'YU'
    a['я'] = 'ya'
    a['ч'] = 'ch'
    a['с'] = 's'
    a['м'] = 'm'
    a['и'] = 'i'
    a['т'] = 't'
    a['ь'] = '\''
    a['б'] = 'b'
    a['ю'] = 'yu'

    for (let i in word) {
        if (word.hasOwnProperty(i)) {
            if (a[word[i]] === undefined) {
                answer += word[i]
            } else {
                answer += a[word[i]]
            }
        }
    }
    return answer
}

const slugify = function (string) {
    const a = 'àáäâãåèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;'
    const b = 'aaaaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------'
    const p = new RegExp(a.split('').join('|'), 'g')

    return string.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
        .replace(/&/g, '-and-') // Replace & with 'and'
        .replace(/[^\w-]+/g, '') // Remove all non-word characters
        .replace(/-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, '') // Trim - from end of text
}

const copyToClipboard = function (string) {
    const el = document.createElement('textarea')
    el.value = string
    document.body.appendChild(el)
    el.select()
    document.execCommand('copy')
    document.body.removeChild(el)
}

export { transliterate, slugify, copyToClipboard }
