import axios from 'axios'
import qs from 'qs'

const instance = axios.create()

instance.defaults.paramsSerializer = params => {
    return qs.stringify(params)
}

instance.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
instance.defaults.headers.common['X-CSRF-TOKEN'] = document.head.querySelector(
    'meta[name="csrf-token"]'
).content


instance.interceptors.response.use(
    response => response,
    error => {
        const { status } = error.response

        // Show the user a 500 error
        if (status >= 500) {
            window.Funky.$emit('error', error.response.data.message)
            return Promise.reject(error)
        }

        // Handle Session Timeouts
        if (status === 401) {
            window.location.href = '/'
            return Promise.reject(error)
        }

        // Handle Forbidden
        if (status === 403) {
            window.Funky.$emit('unauthorized', error.response.data.message)
            return Promise.reject(error)
        }

        // Handle Token Timeouts
        if (status === 419) {
            window.Funky.$emit('token-expired')
            return Promise.reject(error)
        }

        if (status === 422) {
            return Promise.reject(error)
        }

        window.Funky.$emit('error', error.response.data.message)
        return Promise.reject(error)
    }
)

export default instance
