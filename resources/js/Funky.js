import Vue from 'vue'

export default class Funky {
    constructor () {
        this.bus = new Vue()
    }

    $on(...args) {
        this.bus.$on(...args)
    }

    $once(...args) {
        this.bus.$once(...args)
    }

    $off(...args) {
        this.bus.$off(...args)
    }

    $emit(...args) {
        this.bus.$emit(...args)
    }
}
