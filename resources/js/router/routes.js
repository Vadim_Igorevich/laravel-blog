import NotFound from '../views/errors/NotFound'

export default [
    {
        path: '/',
        name: 'dashboard',
        component: () => import('../views/dashboard'),
    },
    {
        path: '/users',
        name: 'users.index',
        component: () => import('../views/users/index')
    },
    {
        path: '/users/create',
        name: 'users.create',
        component: () => import('../views/users/create'),
    },
    {
        path: '/users/:id/edit',
        name: 'users.edit',
        component: () => import('../views/users/edit'),
    },
    {
        path: '/settings/change-password',
        name: 'settings.changePassword',
        component: () => import('../views/settings/change-password'),
    },
    {
        path: '/posts',
        name: 'posts.index',
        component: () => import('../views/posts/index')
    },
    {
        path: '/posts/create',
        name: 'posts.create',
        component: () => import('../views/posts/create'),
    },
    {
        path: '/posts/:slug',
        name: 'posts.show',
        component: () => import('../views/posts/show'),
    },
    {
        path: '/posts/:slug/edit',
        name: 'posts.edit',
        component: () => import('../views/posts/edit'),
    },
    {
        path: '*',
        component: NotFound
    },
]
