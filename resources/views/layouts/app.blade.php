<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

    <!-- Libs CSS -->
    <link rel="stylesheet" href="{{ asset('assets/fonts/feather/feather.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/libs/flatpickr/dist/flatpickr.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/libs/quill/dist/quill.core.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/libs/highlightjs/styles/vs2015.css') }}"/>

    <!-- Map -->
    <link href="https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.css" rel="stylesheet"/>

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/theme.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/libs/vue-multiselect/dist/vue-multiselect.min.css') }}">

    <!-- App CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title -->
    <title>{{ config('app.name') }}</title>

    <script>
        window.config = @json($config)
    </script>
</head>
<body>
<div id="app">
@auth
    <!-- Hidden logout form -->
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>

        <!-- NAVIGATION -->
        <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light" id="sidebar">
            <div class="container-fluid">
                <!-- Toggler -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidebarCollapse"
                        aria-controls="sidebarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Brand -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('assets/img/logo.png') }}" class="navbar-brand-img mx-auto"
                         alt="{{ config('app.name') }}">
                </a>

                <!-- User (xs) -->
                <div class="navbar-user d-md-none">
                    <!-- Dropdown -->
                    <div class="dropdown">
                        <!-- Toggle -->
                        <a href="#" id="sidebarIcon" class="dropdown-toggle" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <div class="avatar avatar-sm">
                                <img src="{{ auth()->user()->avatar_url }}" class="avatar-img rounded-circle" alt="...">
                            </div>
                        </a>

                        <!-- Menu -->
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="sidebarIcon">
                            <div class="dropdown-item">
                                {{ auth()->user()->name }}
                            </div>
                            <hr class="dropdown-divider">
                            {{--                            <a href="#" class="dropdown-item">Профиль</a>--}}
                            @can('viewAny', App\Models\User::class)
                                <router-link :to="{ name: 'users.index' }" class="dropdown-item">
                                    <i class="fe fe-users mr-2"></i>
                                    <span>Пользователи</span>
                                </router-link>
                            @endcan
                            <router-link :to="{ name: 'settings.changePassword' }" class="dropdown-item">
                                <i class="fe fe-settings mr-2"></i>
                                Настройки
                            </router-link>
                            <hr class="dropdown-divider">
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                               class="dropdown-item">Выход</a>
                        </div>
                    </div>
                </div>

                <!-- Collapse -->
                <div class="collapse navbar-collapse" id="sidebarCollapse">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <router-link :to="{ name: 'dashboard' }" class="nav-link" exact-active-class="active">
                                <i class="fe fe-activity"></i>
                                Главная
                            </router-link>
                        </li>
                        @can('viewAny', App\Models\User::class)
                            <li class="nav-item">
                                <router-link :to="{ name: 'users.index' }" class="nav-link" active-class="active">
                                    <i class="fe fe-users mr-2"></i>
                                    <span>Пользователи</span>
                                </router-link>
                            </li>
                        @endcan
                        @can('viewAny', App\Models\Post::class)
                            <li class="nav-item">
                                <router-link :to="{ name: 'posts.index' }" class="nav-link" active-class="active">
                                    <i class="fe fe-book mr-2"></i>
                                    <span>Новости</span>
                                </router-link>
                            </li>
                        @endcan
                    </ul>

                    <!-- Push content down -->
                    <div class="mt-auto"></div>

                    <!-- User (md) -->
                    <div class="navbar-user d-none d-md-flex" id="sidebarUser">
                        <!-- Dropup -->
                        <div class="dropup">
                            <!-- Toggle -->
                            <a href="#" id="sidebarIconCopy" class="dropdown-toggle" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="avatar avatar-sm">
                                    <img src="{{ auth()->user()->avatar_url }}" class="avatar-img rounded-circle"
                                         alt="...">
                                </div>
                            </a>

                            <!-- Menu -->
                            <div class="dropdown-menu" aria-labelledby="sidebarIconCopy">
                                <div class="dropdown-item">
                                    {{ auth()->user()->name }}
                                </div>
                                <hr class="dropdown-divider">
                                {{-- <a href="#" class="dropdown-item">Профиль</a>--}}
                                @can('create', App\Models\User::class)
                                    <router-link :to="{ name: 'users.index' }" class="dropdown-item">
                                        <i class="fe fe-users mr-2"></i>
                                        <span>Пользователи</span>
                                    </router-link>
                                @endcan
                                <router-link :to="{ name: 'settings.changePassword' }" class="dropdown-item">
                                    <i class="fe fe-settings mr-2"></i>
                                    Настройки
                                </router-link>
                                <hr class="dropdown-divider">
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                   class="dropdown-item">Выход</a>
                            </div>
                        </div>
                    </div>
                </div> <!-- / .navbar-collapse -->
            </div>
        </nav>
    @endauth

    @yield('content')
</div>

<!-- JAVASCRIPT
================================================== -->
<!-- Libs JS -->
<script src="{{ asset('assets/libs/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/libs/@shopify/draggable/lib/es5/draggable.bundle.legacy.js') }}"></script>
<script src="{{ asset('assets/libs/autosize/dist/autosize.min.js') }}"></script>
<script src="{{ asset('assets/libs/chart.js/dist/Chart.min.js') }}"></script>
<script src="{{ asset('assets/libs/dropzone/dist/min/dropzone.min.js') }}"></script>
<script src="{{ asset('assets/libs/flatpickr/dist/flatpickr.min.js') }}"></script>
<script src="{{ asset('assets/libs/highlightjs/highlight.pack.min.js') }}"></script>
<script src="{{ asset('assets/libs/jquery-mask-plugin/dist/jquery.mask.min.js') }}"></script>
<script src="{{ asset('assets/libs/list.js/dist/list.min.js') }}"></script>
<script src="{{ asset('assets/libs/quill/dist/quill.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/libs/chart.js/Chart.extension.js') }}"></script>

<!-- Map -->
<script src='https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.js'></script>

<!-- Theme JS -->
<script src="{{ asset('assets/js/theme.min.js') }}"></script>
<script src="{{ asset('assets/js/dashkit.min.js') }}"></script>

<!-- App JS -->

<script src="{{ mix('js/app.js') }}" defer></script>

</body>
</html>
