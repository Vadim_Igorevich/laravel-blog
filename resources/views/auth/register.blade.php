@extends('layouts.app')

@section('content')
    <div class="vh-100 d-flex align-items-center bg-auth border-top border-top-2 border-primary">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-5 col-xl-4 my-5">

                    <!-- Heading -->
                    <h1 class="display-4 text-center mb-3">
                        {{ __('Register') }}
                    </h1>

                    <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <!-- Name -->
                        <div class="form-group">
                            <label for="name">{{ __('Name') }}</label>

                            <input id="name"
                                   type="text"
                                   class="form-control @error('name') is-invalid @enderror"
                                   name="name"
                                   value="{{ old('name') }}"
                                   required
                                   autocomplete="name"
                                   autofocus>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <!-- E-Mail Address -->
                        <div class="form-group">
                            <label for="email">{{ __('E-Mail Address') }}</label>

                            <input id="email"
                                   type="email"
                                   class="form-control @error('email') is-invalid @enderror"
                                   name="email"
                                   value="{{ old('email') }}"
                                   required
                                   autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <!-- Password -->
                        <div class="form-group">
                            <label for="password">{{ __('Password') }}</label>

                            <input id="password"
                                   type="password"
                                   class="form-control @error('password') is-invalid @enderror"
                                   name="password"
                                   required
                                   minlength="8"
                                   autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <!-- Confirm Password -->
                        <div class="form-group">
                            <label for="password-confirm">{{ __('Confirm Password') }}</label>

                            <input id="password-confirm"
                                   type="password"
                                   class="form-control"
                                   name="password_confirmation"
                                   required
                                   autocomplete="new-password">
                        </div>

                        <!-- Submit -->
                        <button type="submit" class="btn btn-lg btn-block btn-primary mb-3">
                            {{ __('Register') }}
                        </button>

                        <!-- Login -->
                        @if (Route::has('login'))
                            <div class="text-center">
                                <small class="text-muted text-center">
                                    Есть аккаунт? <a href="{{ route('login') }}">Войдите</a>.
                                </small>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
