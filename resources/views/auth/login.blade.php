@extends('layouts.app')

@section('content')
    <div class="vh-100 d-flex align-items-center bg-auth border-top border-top-2 border-primary">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-5 col-xl-4 my-5">

                    <!-- Heading -->
                    <h1 class="display-4 text-center mb-3">
                        Вход
                    </h1>

                    <!-- Subheading -->
                    <p class="text-muted text-center mb-5">
                        Используйте ваш электронный адрес и пароль для входа в систему.
                    </p>

                    <!-- Form -->
                    <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <!-- Email address -->
                        <div class="form-group">

                            <label>{{ __('E-Mail Address') }}</label>

                            <input id="email"
                                   type="email"
                                   class="form-control @error('email') is-invalid @enderror"
                                   name="email"
                                   value="{{ old('email') }}"
                                   required
                                   autocomplete="email"
                                   autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                            @enderror

                        </div>

                        <!-- Password -->
                        <div class="form-group">

                            <div class="row">
                                <div class="col">

                                    <!-- Label -->
                                    <label>Пароль</label>

                                </div>
                                <div class="col-auto">

                                @if (Route::has('password.request'))
                                    <!-- Help text -->
                                        <a href="{{ route('password.request') }}" class="form-text small text-muted" tabindex="-1">
                                            Забыли пароль?
                                        </a>
                                    @endif

                                </div>
                            </div> <!-- / .row -->

                            <!-- Input group -->
                            <div class="input-group input-group-merge">

                                <!-- Input -->
                                <input type="password"
                                       name="password"
                                       autocomplete="current-password"
                                       class="form-control  @error('password') is-invalid @enderror form-control-appended">

                                <!-- Icon -->
                                <div class="input-group-append">
                                  <span class="input-group-text">
                                      <i class="fe fe-lock"></i>
                                  </span>
                                </div>

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    {{ $message }}
                                </span>
                                @enderror

                            </div>
                        </div>

                        <!-- Remember Me -->
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox"
                                       name="remember"
                                       class="custom-control-input"
                                       id="remember"
                                    {{ old('remember', true) ? 'checked' : '' }}>
                                <label class="custom-control-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>

                        <!-- Submit -->
                        <button class="btn btn-lg btn-block btn-primary mb-3">
                            Войти
                        </button>

                        <!-- Register -->
                        @if (Route::has('register'))
                            <div class="text-center">
                                <small class="text-muted text-center">
                                    Нет аккаунта? <a href="{{ route('register') }}">Зарегистрируйтесь</a>.
                                </small>
                            </div>
                        @endif

                    </form>

                </div>
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </div>
@endsection
